package com.wojciech.opengategsm;

import android.content.Context;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class PhoneActivity extends AppCompatActivity {

    Button bWyslij;
    static String phoneNr;
    static EditText etPhoneNr;
    static EditText etText;
    static CheckBox cbText;
    private static Context context;
    ConstraintLayout constraintLayout;
    ConstraintSet constraintSet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PhoneActivity.context = getApplicationContext();
        setContentView(R.layout.activity_phone);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        constraintLayout = (ConstraintLayout) findViewById(R.id.content_phone);
        cbText = findViewById(R.id.checkBox);
        etText = findViewById(R.id.textView3);
        etText.setVisibility(View.GONE);
        bWyslij = findViewById(R.id.button);
        etPhoneNr = (EditText) findViewById(R.id.editText);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // ustawienie widoku wysylania SMS
        cbText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cbText.isChecked())
                {
                    etText.setVisibility(View.VISIBLE);
                }
                else
                {
                    etText.setVisibility(View.GONE);
                }
            }
        });
    }

    public void clickWyslij(View view) {
        try {
            phoneNr = getPhoneNr(); //nie mozna pobrac nr telefonu poniewaz go nie podano

            if (PhoneActivity.cbText.isChecked()) {
                try {
                    SendSMS.sendSMS(etPhoneNr.getText().toString(), getPhoneActivityContext(), getetText());
                }
                catch (Exception e) {
                    Toast.makeText(context, "Nie podano żadnej treści", Toast.LENGTH_LONG).show();
                }
            }
            else
                SendSMS.sendSMS(etPhoneNr.getText().toString(), getPhoneActivityContext(), "2988");
        }
        catch (Exception e) {
            Toast.makeText(context, "Nie podano numeru telefonu", Toast.LENGTH_LONG).show();
        }
    }

    public static Context getPhoneActivityContext() {
        return PhoneActivity.context;
    }

    public static String getPhoneNr()
    {
        return "+48" + etPhoneNr.getText().toString();
    }

    public static String getetText()
    {
        return etText.getText().toString();
    }

}
