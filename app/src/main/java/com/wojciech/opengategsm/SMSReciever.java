package com.wojciech.opengategsm;


import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.SmsMessage;
import android.widget.Toast;

public   class   SMSReciever   extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        Bundle data = intent.getExtras(); //zmiana pliku z typu intent na bundle

        Object[] pdus = (Object[]) data.get("pdus"); //zapisanie obiektu pdus z data

        SmsMessage sms = SmsMessage.createFromPdu((byte[]) pdus[0]);

        ActivityManager am = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE); //wzięcie bierzącego activity
        ComponentName cn = am.getRunningTasks(1).get(0).topActivity;

        try {

            if (PhoneActivity.phoneNr.equals(sms.getOriginatingAddress()) == true) //jeżeli SMS pochodzi od bramy to go zapisz
            {
                MainActivity.writeToFile( "Odebrano SMS:\nNumer telefonu: " + sms.getOriginatingAddress() + "\nTresc:\n" + sms.getMessageBody(), false);
                savedata(context, sms);
                Toast.makeText(context, "Dane zapisane", Toast.LENGTH_LONG).show();

                if (cn.getClassName().equals(PhoneActivity.class.getName())) {
                    Intent smsIntent = new Intent(context, MainActivity.class);
                    smsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(smsIntent);
                }
                else {
                    Intent i = new Intent("gate data");
                    context.sendBroadcast(i);
                }

            }

        }
        catch (Exception e) {}
    }

    public void savedata(Context context, SmsMessage sms)
    {
        MainActivity.writeToFile("Zapis danych do pamieci nieulotnej", false);
        String result[];
        result = sms.getMessageBody().split("\n");
        SharedPreferences mydata = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = mydata.edit();
        editor.putString("phone", sms.getOriginatingAddress());
        editor.putString("idGate", result[0]);
        editor.putString("code", result[1]);
        editor.putString("latitude", result[2]);
        editor.putString("longitude", result[3]);
        editor.putString("all", sms.getDisplayMessageBody());
        editor.commit();
    }
}



