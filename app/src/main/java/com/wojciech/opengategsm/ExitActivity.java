package com.wojciech.opengategsm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;


public class ExitActivity extends AppCompatActivity {

    private Button bBackground, bClose, bBackToApp;
    private boolean close = false;
    private boolean destroy = false;
    public BroadcastReceiver mReceiver;
    NotificationManager notificationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //nie wyswietla OpenGateGSM (tytulu na gorze ekranu)
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.exit);

        bBackToApp = (Button) findViewById(R.id.bBackToApp);
        bBackground = (Button) findViewById(R.id.bBackground);
        bClose = (Button) findViewById(R.id.bClose);

        bBackToApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                close = true;
                onBackPressed();
            }
        });

        bBackground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.writeToFile("Tryb: Dzialanie w tle", false);
                Intent i = new Intent(getApplicationContext(), Service_GPS.class);
                startService(i);
                notificationSet();
                moveTaskToBack(true);
                Intent intent = new Intent(ExitActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        bClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.writeToFile("Zamknij aplikacje", false);
                Intent i = new Intent(getApplicationContext(), Service_GPS.class);
                stopService(i);
                ExitApp.exitApplication(getApplicationContext());
            }
        });

    }

    @Override
    public void onBackPressed() {

        //podwojne nacisniecie przycisku back na dole ekranu
        if (close == true) {
            super.onBackPressed();
            close = false;
            destroy = true;
        }
        //zamkniecie aplikacji
        else {
            Intent i = new Intent(getApplicationContext(), Service_GPS.class);
            stopService(i);
            Log.d("aktywnosc", "notificationManager");
            ExitApp.exitApplication(getApplicationContext());
        }
    }

    //nieusuwalne powiadomianie o wlaczonej aplikacji
    public void notificationSet() {

        Intent intent = new Intent("notification");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, (int) System.currentTimeMillis(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                .setContentTitle("OpenGateGSM")
                .setContentText("Kliknij aby zamknąć")
                .setSmallIcon(android.R.drawable.ic_menu_mylocation)
                .setContentIntent(pendingIntent)
                .setOngoing(true); //Ongoing powoduje ze nie mozna zamknac powiadomienia


        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification.build());

        //jezeli kliknie sie powiadomienie to usuwa powiadomianie i zamyka aplikacje
        if (mReceiver == null) {
            mReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    Log.d("aktywnosc", "odbiornik");
                    Intent i = new Intent(getApplicationContext(), Service_GPS.class);
                    stopService(i);
                    ExitApp.exitApplication(MainActivity.getMainActivityContext());
                    notificationManager.cancel(0);
                }
            };
        }
        registerReceiver(mReceiver, new IntentFilter("notification"));
    }

    @Override
    protected void onDestroy() {
        Log.d("aktywnosc", "onDestroyExitActivity");
        super.onDestroy();
        if (destroy != true) { //aby nie wyrzucalo bledu przy powrocie do aplikacji
            //aby po wejsciu w tryb dzialania w tle i potem po powrocie do aplikacji jak chcialo sie ja zamknac
            // to nie zamykało sie powiadomienie
            notificationManager.cancel(0);
            destroy = false;
        }
        if (mReceiver != null) {
            unregisterReceiver(mReceiver);
        }
    }
}


