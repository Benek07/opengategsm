package com.wojciech.opengategsm;

//obliczanie dystansu do bramy
public class Distance {

    public static double getDistance(double latitude1, double longitude1, double latitude2, double longitude2)
    {
        return Math.sqrt(Math.pow((latitude2 - latitude1),2) + Math.pow(Math.cos(latitude1*Math.PI/180)*(longitude2-longitude1),2))*40075.704/360*1000;
    }

}
