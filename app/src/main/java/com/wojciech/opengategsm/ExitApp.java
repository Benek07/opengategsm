package com.wojciech.opengategsm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class ExitApp extends MainActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if(android.os.Build.VERSION.SDK_INT >= 21)
        {
            finishAndRemoveTask();
        }
        else
        {
            Log.d("aktywnosc", "ExitApp");
            finish();
            System.exit(0);
        }
    }

    public static void exitApplication(Context context)
    {
        Intent intent = new Intent(context, ExitApp.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK  | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        context.startActivity(intent);
    }
}
