package com.wojciech.opengategsm;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;
import android.widget.Toast;

public class SendSMS {

    private static final int MY_PERMISSIONS_REQUEST_SEND_SMS =0;

    public static void sendSMS(String phoneNr, Context context, String message) {

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.SEND_SMS)) {
                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.SEND_SMS},
                        MY_PERMISSIONS_REQUEST_SEND_SMS);
            } else {
                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.SEND_SMS},
                        MY_PERMISSIONS_REQUEST_SEND_SMS);
            }
        }
        if (!phoneNr.equals("")) {
            MainActivity.writeToFile("Wyslano SMS na numer " + phoneNr + ", o tresci: " + message, true);
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNr, null, message, null, null);
            Toast.makeText(context, "Wysłano SMS", Toast.LENGTH_LONG).show();
        }
    }


    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_SEND_SMS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(PhoneActivity.getPhoneActivityContext(), "Dostep otrzymany",
                            Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(PhoneActivity.getPhoneActivityContext(),
                            "Nie udało się wysłac SMS", Toast.LENGTH_LONG).show();
                    return;
                }
            }
        }

    }
}
