package com.wojciech.opengategsm;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;


import java.text.DecimalFormat;

import static com.wojciech.opengategsm.MainActivity.getMainActivityContext;
import static com.wojciech.opengategsm.MainActivity.latitude;
import static com.wojciech.opengategsm.MainActivity.longitude;
import static com.wojciech.opengategsm.MainActivity.phone;

public class Service_GPS extends Service {


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    static String textLocation;
    static String textDistance;
    double distanceValue;
    private Distance distance;
    boolean distanceFlag = false;
    LocationManager managerLocation;
    LocationListener listenerLocation;
    private static final int MY_PERMISSION_ACCESS_COARSE_LOCATION = 11;
    private static final int MY_PERMISSION_ACCESS_FINE_LOCATION = 12;
    int flagGps = 0;

    @Override
    public void onCreate() {
        listenerLocation = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) { //wywoluje funkcje przy zmianie lokalizacji
                location.getLatitude(); //szerokosc geograficzna
                location.getLongitude(); //dlugosc geograficzna

                if (longitude.equals("") == false || longitude == null) {
                    distanceValue = distance.getDistance(Double.parseDouble(latitude), Double.parseDouble(longitude), location.getLatitude(), location.getLongitude());
                    DecimalFormat df = new DecimalFormat("#.##"); //zamiana na 2 miejsca po przecinku
                    textDistance = (Double.toString(Double.parseDouble(df.format(distanceValue))));
                    textLocation = "szerokosc: " + location.getLatitude() + "\nDlugosc: " + location.getLongitude() + "\nodleglosc:" + textDistance;
                    if (distanceValue < 100 && distanceFlag) {
                        MainActivity.writeToFile("Wyslanie SMS, ktory otwiera brame brame\n" +
                                "Szerokosc geograficzna: " + location.getLatitude() + "\nDlugosc geograficzna: " + location.getLongitude() + "\nOdleglosc:" + textDistance, false);
                        SendSMS.sendSMS(phone, getMainActivityContext(), MainActivity.code);
                        distanceFlag = false;
                    } else if (distanceValue > 125) {
                        distanceFlag = true;
                    }

                }
                Intent i = new Intent("location");
                i.putExtra("textCoordinates", "szerokosc: " + location.getLatitude() + "\ndlugosc: " + location.getLongitude() + "\nodleglosc: " + textDistance);
                sendBroadcast(i);

//                if (distanceValue >= 30 && distanceValue < 500) {flagGps = true;}

                if (ActivityCompat.checkSelfPermission(getMainActivityContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getMainActivityContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }

                MainActivity.writeToFile("Flaga: " + flagGps + "\nSzerokosc geograficzna: " + location.getLatitude() + "\nDlugosc geograficzna: " + location.getLongitude() + "\nOdleglosc:" + textDistance, false);

                if (distanceValue < 100 && flagGps != 1) {
                    managerLocation.removeUpdates(listenerLocation);
                    MainActivity.writeToFile("Jezeli uzytkownik jest w odleglosci mniejszej od 100 metrow to aktualizuj polozenie GPS po przesunieciu o minimum 1 metr\nflaga: " + flagGps +
                            "Szerokosc geograficzna: " + location.getLatitude() + "\nDlugosc geograficzna: " + location.getLongitude() + "\nOdleglosc:" + textDistance, false);
                    managerLocation.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 1, listenerLocation); //minimalny czas i odleglosc po ktorej nastapi aktualizacja
                    flagGps = 1;
                } else if (distanceValue >= 100 && distanceValue < 500 && flagGps != 2) {

                    managerLocation.removeUpdates(listenerLocation);
                    MainActivity.writeToFile("Jezeli uzytkownik jest w odleglosci od 100 do 500 metrow to aktualizuj polozenie GPS po przesunieciu o minimum 20 metrow\nflaga: " + flagGps +
                            "Szerokosc geograficzna: " + location.getLatitude() + "\nDlugosc geograficzna: " + location.getLongitude() + "\nOdleglosc:" + textDistance, false);
                    managerLocation.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 20, listenerLocation); //minimalny czas i odleglosc po ktorej nastapi aktualizacja
                    flagGps = 2;
                } else if (distanceValue >= 500 && distanceValue < 3000 && flagGps != 3) {
                    managerLocation.removeUpdates(listenerLocation);
                    MainActivity.writeToFile("Jezeli uzytkownik jest w odleglosci od 500 do 3000 metrow to aktualizuj polozenie GPS po przesunieciu o minimum 100 metrow\nflaga: " + flagGps +
                            "Szerokosc geograficzna: " + location.getLatitude() + "\nDlugosc geograficzna: " + location.getLongitude() + "\nOdleglosc:" + textDistance, false);
                    managerLocation.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 100, listenerLocation); //minimalny czas i odleglosc po ktorej nastapi aktualizacja
                    flagGps = 3;
                } else if (distanceValue >= 3000 && distanceValue < 10000 && flagGps != 4) {
                    managerLocation.removeUpdates(listenerLocation);
                    MainActivity.writeToFile("Jezeli uzytkownik jest w odleglosci od 3 do 10 kilometrow to aktualizuj polozenie GPS po przesunieciu o minimum 1 kilometr\nflaga: " + flagGps +
                            "Szerokosc geograficzna: " + location.getLatitude() + "\nDlugosc geograficzna: " + location.getLongitude() + "\nOdleglosc:" + textDistance, false);
                    managerLocation.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 1000, listenerLocation); //minimalny czas i odleglosc po ktorej nastapi aktualizacja
                    flagGps = 4;
                } else if (distanceValue >= 10000 && distanceValue < 50000 && flagGps != 5) {
                    MainActivity.writeToFile("Jezeli uzytkownik jest w odleglosci od 10 do 50 kilometr to aktualizuj polozenie GPS po przesunieciu o minimum 5 kilometrow\nflaga: " + flagGps +
                            "Szerokosc geograficzna: " + location.getLatitude() + "\nDlugosc geograficzna: " + location.getLongitude() + "\nOdleglosc:" + textDistance, false);
                    managerLocation.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 5000, listenerLocation); //minimalny czas i odleglosc po ktorej nastapi aktualizacja
                    flagGps = 5;
                } else if (distanceValue >= 50000 && flagGps != 6) {
                    MainActivity.writeToFile("Jezeli uzytkownik jest w odleglosci powyżej 50 kilometr to aktualizuj polozenie GPS po przesunieciu o minimum 20 kilometrow\nflaga: " + flagGps +
                            "Szerokosc geograficzna: " + location.getLatitude() + "\nDlugosc geograficzna: " + location.getLongitude() + "\nOdleglosc:" + textDistance, false);
                    managerLocation.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 20000, listenerLocation); //minimalny czas i odleglosc po ktorej nastapi aktualizacja
                    flagGps = 6;
                }
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {
                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);

            }
        };

//
//
        managerLocation = (LocationManager) getSystemService(Context.LOCATION_SERVICE); //zapewnia dostep do uslug lokalizacyjnych systemu
        //GPS_PROVIDER okresla lokalizacje za pomoca satelit
//

//
        if (distanceValue < 100) {
            MainActivity.writeToFile("flaga =0 ", false);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            managerLocation.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 1, listenerLocation); //minimalny czas i odleglosc po ktorej nastapi aktualizacja
        }



    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if(managerLocation != null){
            //noinspection MissingPermission
            managerLocation.removeUpdates(listenerLocation);
        }
    }



    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSION_ACCESS_COARSE_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                } else {
                    // permission denied
                }
                break;
            }

            case MY_PERMISSION_ACCESS_FINE_LOCATION: {
                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        // permission was granted
                    } else {
                        // permission denied
                    }
                    break;
                }

            }
        }
}



