package com.wojciech.opengategsm;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    static private String path = Environment.getExternalStorageDirectory().toString() + "/OpenGateGSMdata/";
    public ListView listGate;
    public ArrayAdapter<String> array;
    private Button bOpen;
    private TextView t1;
    private static Context context;
    boolean saveFlag = true;
    static String phone, code, latitude, longitude;
    static final String close = "close";
    String all;
    private BroadcastReceiver broadcastReceiver, broadcasrReceiverGateData;
    static File file = new File(path + "/danebram.txt");

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        MainActivity.context = getApplicationContext();
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        t1 = (TextView) findViewById(R.id.location);
        setSupportActionBar(toolbar);
        bOpen = (Button) findViewById(R.id.bOpen);

        createDirectory(context);
        createFile(context);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, PhoneActivity.class);
                startActivity(intent);
            }
        });

        bOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SendSMS.sendSMS(phone, getMainActivityContext(), MainActivity.code);
            }
        });

        if(runtime_permissions()) {
            Toast.makeText(context, "otrzymano uprawnienia dla GPS", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onDestroy() {

        Log.d("aktywnosc", "onDestroy");
        super.onDestroy();
        // usuwanie broadcasta
        if(broadcastReceiver != null){
            unregisterReceiver(broadcastReceiver);
        }
    }

    @Override
    protected void onPause()  {
        super.onPause();
        Log.d("aktywnosc", "onPause");
    }

    @Override
    protected void onRestart()  {
        super.onRestart();
        Log.d("aktywnosc", "onRestart");
    }

    @Override
    protected void onStop()  {
        super.onStop();
        Log.d("aktywnosc", "onStop");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("aktywnosc", "onResume");
        // wyslanie broadcastowo lokalizacji GPS na ekran główny
        if(broadcastReceiver == null){
            broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    t1.setText("\n" +intent.getExtras().get("textCoordinates"));
                }
            };
        }
        registerReceiver(broadcastReceiver,new IntentFilter("location")); //rejestracja broadcast


        if(broadcasrReceiverGateData == null){
            broadcasrReceiverGateData = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    onResume();
                }
            };
        }
        // wyswietlenia dane o bramie z SMS
        registerReceiver(broadcasrReceiverGateData,new IntentFilter("gate data"));


        SharedPreferences mydata = PreferenceManager.getDefaultSharedPreferences(context);
        // try bo mydata.getString może wyrzucić wyjatek jezeli nic nie bedzie zapisane do phone
        try
        {   //tworzenie tablicy do zapisu danych z bramy
            //warunek aby na pocztku programu nie bylo pustego pola przy itemView
            if (mydata.getString("phone", "").equals("") == false && saveFlag) {
                listGate = (ListView) findViewById(R.id.list);
                ArrayList<String> gateArrayList = new ArrayList<String>();
                array = new ArrayAdapter<String>(this, R.layout.content_main, R.id.row, gateArrayList);
                listGate.setAdapter(array);
                saveFlag = false;
                }

            phone = mydata.getString("phone", "");
            code = mydata.getString("code", "");
            latitude = mydata.getString("latitude", "");
            longitude = mydata.getString("longitude", "");
            all = mydata.getString("all", "");

            //aby nie zostawalo puste pole po skasowaniu zmiennych to czyszcze tablice
            if (mydata.getString("phone", "").equals("")) {
                array.clear();
            }
            else
            {
                bOpen.setVisibility(View.VISIBLE); //przycisk wyslij
                array.clear();
                array.add(phone + "\n" + all);
            }
        }
        // jezeli wyjątek to tablica nie zostanie stworzona
        catch (Exception e) {bOpen.setVisibility(View.GONE);}
    }

    @Override
    public void onBackPressed() {

        Log.d("aktywnosc", "Back button");
        // uruchomienie ekranu z wyborem zamknij, dzialaj w tle, wroc do aplikacji
        Intent intent = new Intent(MainActivity.this, ExitActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // Ikonka kosza
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        //ikona kosza - delete
        if (id == R.id.action_settings) {
            Log.d("przycisk", "delete");
            phone = code = latitude = longitude = all = "";
            SharedPreferences mydata = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = mydata.edit();
            editor.remove("phone");
            editor.remove("idGate");
            editor.remove("code");
            editor.remove("latitude");
            editor.remove("longitude");
            editor.remove("all");
            editor.commit();
            array.clear();
            bOpen.setVisibility(View.GONE);

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static Context getMainActivityContext() {
        return MainActivity.context;
    }

    protected boolean runtime_permissions() { //przyzwolenia dla GPS dla Androida > wersji 23
        if(Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){

            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},100);

            return true;
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == 100){
            if( grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED){

            }else {
                runtime_permissions();
            }
        }
    }

    public void createDirectory(Context context) //tworzenie folderu na dane
    {
        File dir = new File(path);
        if (!dir.exists()) //jesli folder nie istenije utwórz go
        {
            try
            {
                dir.mkdir();
            }
            catch (Exception e)
            {
                Toast.makeText(context, "utworzenie folderu nie powiodlo sie" + e.toString(), Toast.LENGTH_LONG).show();
            }
        }

    }

    public static void createFile(Context context)
    {
        if (!file.exists()) {
            FileOutputStream output; //otwarcie pliku do zapisu
            OutputStreamWriter fileWriter; //zamienia strumien znakow na strumien bajtow
            try {
                output = new FileOutputStream(file);
                fileWriter = new OutputStreamWriter(output);
                fileWriter.append("Start aplikacji\n");
                fileWriter.close();
                output.close();

            } catch (Exception e) {
                Toast.makeText(context, "utworzenie pliku nie powiodlo sie" + e.toString(), Toast.LENGTH_LONG).show();
            }
        }
    }
    
      public static void writeToFile(String text, Boolean saveTime)
      {
          try {

              if (saveTime == true) {
                  Calendar c = Calendar.getInstance();
                  SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                  Writer output = new BufferedWriter(new FileWriter(path + "/danebram.txt", true));
                  output.write(text + ",  " + df.format(c.getTime()) + '\n' );
                  output.close();
              }
              else
              {
                  Writer output = new BufferedWriter(new FileWriter(path + "/danebram.txt", true));
                  output.write(text + '\n');
                  output.close();
              }

          }
          catch (Exception e)
          { }
      }
}
